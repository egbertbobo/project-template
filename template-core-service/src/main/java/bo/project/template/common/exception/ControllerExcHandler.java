package bo.project.template.common.exception;

import bo.project.template.exception.SelfErrorCode;
import bo.project.template.exception.SelfException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerExcHandler {
    @ExceptionHandler(SelfException.class)
    public ResponseEntity<ErrorResponse> handleSelfException(SelfException e) {
        ErrorResponse error = new ErrorResponse();
        error.setCode(e.getErrorCode().getCode());
        error.setDescription(e.getErrorCode().getDescription());
        error.setMessage(e.getMessage());
        return ResponseEntity.status(e.getHttpStatus()).body(error);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleSelfException(Exception e) {
        ErrorResponse error = new ErrorResponse();
        error.setCode(SelfErrorCode.INTERNAL_SERVER_ERROR.getCode());
        error.setDescription(SelfErrorCode.INTERNAL_SERVER_ERROR.getDescription());
        error.setMessage(e.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }
}
