package bo.project.template.remote.feign;

import org.apache.http.HttpException;
import org.apache.http.client.HttpResponseException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "service-name")
public interface OtherServiceFeignClient {
    @GetMapping("/api/v1/some-endpoint")
    String someEndpoint() throws HttpResponseException;
}
