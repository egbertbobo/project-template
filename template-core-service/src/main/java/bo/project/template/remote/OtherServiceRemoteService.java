package bo.project.template.remote;

import bo.project.template.dao.TemplateDaoImpl;
import bo.project.template.template.client.TemplateClient;
import bo.project.template.template.client.TemplateOthersClient;
import bo.project.template.exception.SelfException;
import bo.project.template.remote.feign.OtherServiceFeignClient;
import org.apache.http.client.HttpResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OtherServiceRemoteService {
    private final static Logger logger = LoggerFactory.getLogger(TemplateDaoImpl.class);
    @Autowired
    private OtherServiceFeignClient otherServiceFeignClient;

    @Autowired
    private TemplateOthersClient templateOthersClient;

    @Autowired
    private TemplateClient templateClient;

    public void remoteCall() {
        try {
            otherServiceFeignClient.someEndpoint();
        } catch (HttpResponseException e) {
            logger.error("This has a error", e);
            throw new SelfException("Error handle, throw self exception.");
        }

    }

    public void remoteCall3() {
        try {
            templateClient.heathCheck();
        } catch (HttpResponseException e) {
            logger.error("This has a error", e);
            throw new SelfException("Error handle, throw self exception.");
        }
    }

    public void remoteCall2() {
        try {
            templateOthersClient.someEndpoint();
        } catch (HttpResponseException e) {
            logger.error("This has a error", e);
            throw new SelfException("Error handle, throw self exception.");
        }
    }
}
