package bo.project.template.mapper;

import bo.project.template.dao.entity.UserTemplatePO;

import java.sql.SQLException;

public interface TemplateMapper {

    UserTemplatePO getUser() throws SQLException;
}
