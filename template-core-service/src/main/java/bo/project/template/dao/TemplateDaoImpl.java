package bo.project.template.dao;

import bo.project.template.exception.SelfException;
import bo.project.template.dao.entity.UserTemplatePO;
import bo.project.template.mapper.TemplateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;

@Repository
public class TemplateDaoImpl implements TemplateDao{
    private final static Logger logger = LoggerFactory.getLogger(TemplateDaoImpl.class);
    @Autowired
    TemplateMapper templateMapper;

    public UserTemplatePO getUser() {
        try {
            return templateMapper.getUser();
        } catch (SQLException e) {
            logger.error("This has a error", e);
            throw new SelfException("Catch SQLException to throw selfException.", e);
        }

    }
}
