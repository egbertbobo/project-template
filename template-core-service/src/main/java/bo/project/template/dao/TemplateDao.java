package bo.project.template.dao;

import bo.project.template.dao.entity.UserTemplatePO;

public interface TemplateDao {
    UserTemplatePO getUser();
}
