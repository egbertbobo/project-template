package bo.project.template.template.client;

import bo.project.template.dto.HeathCheckDTO;
import org.apache.http.client.HttpResponseException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "template", url = "http://localhost:8700/template")
public interface TemplateClient {
    @GetMapping("/v1/ping")
    HeathCheckDTO heathCheck() throws HttpResponseException;
}
