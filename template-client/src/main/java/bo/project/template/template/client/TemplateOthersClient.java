package bo.project.template.template.client;

import bo.project.template.dto.HeathCheckDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
@Component
public class TemplateOthersClient {
    public HeathCheckDTO someEndpoint() throws HttpResponseException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(URI.create("http://localhost:8700/template/v1/ping"));
        HeathCheckDTO heathCheckDTO = null;
        try {
            // 执行请求并获取响应
            HttpResponse response = httpClient.execute(httpGet);

            // 获取响应状态码
            int statusCode = response.getStatusLine().getStatusCode();
            System.out.println("Status Code: 谢谢谢谢：更改。测试" + statusCode);


            // 获取响应内容
            HttpEntity entity = response.getEntity();
            ObjectMapper objectMapper = new ObjectMapper();
            heathCheckDTO = objectMapper.readValue(entity.getContent(), HeathCheckDTO.class);
            httpClient.close();

            // 关闭连接

        } catch (IOException e) {
            e.printStackTrace();
        }
        return heathCheckDTO;
    }
}
