package bo.project.template.exception;

public class SelfException extends RuntimeException {
    private int httpStatus;
    private SelfErrorCode errorCode;

    public SelfException(String errorMessage) {
        super(errorMessage);
        this.errorCode = SelfErrorCode.INTERNAL_SERVER_ERROR;
        this.httpStatus = 500;
    }

    public SelfException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
        this.errorCode = SelfErrorCode.INTERNAL_SERVER_ERROR;
        this.httpStatus = 500;
    }

    public SelfException(int httpStatus, String errorMessage, SelfErrorCode selfErrorCode, Throwable cause) {
        super(errorMessage, cause);
        this.errorCode = selfErrorCode;
        this.httpStatus = httpStatus;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public SelfErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(SelfErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
