package bo.project.template.exception;

public enum SelfErrorCode {
    // 4XX

    // 5XX
    INTERNAL_SERVER_ERROR(500001, "A internal server error occurred.");

    private int code;
    private String description;

    SelfErrorCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
